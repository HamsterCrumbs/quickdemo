﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPuddle : MonoBehaviour {

    SpriteRenderer rend;
    Texture2D tex;
    Color[] colorArray;

    // Use this for initialization
    void Start () {
        Debug.Log("INITIALIZING PUDDLE");
        rend = GetComponent<SpriteRenderer>();

        tex = new Texture2D(24, 27);
        colorArray = new Color[tex.width * tex.height];

        for (int x = 0; x < tex.width; x++)
        {
            for (int y = 0; y < tex.height; y++)
            {
                float distanceToEdge = Mathf.Min(x, tex.width - x, y, tex.height - y);

                colorArray[x + (y * tex.width)] = new Color((float)50 / 255, (float)84 / 255, (float)56 / 255, Random.Range(0.8f, 0.9f));
            }
        }
        tex.SetPixels(colorArray);
        tex.Apply();
        tex.wrapMode = TextureWrapMode.Clamp;
        tex.filterMode = FilterMode.Point;

        Sprite newSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one * 0.5f, 1);
        rend.sprite = newSprite;
	}

    public void eraseByBoxCollider(GameObject eraser, float intensity) {
        Debug.Log("erasing: bounds-" + eraser.GetComponent<BoxCollider2D>().bounds.center + " puddle-" + transform.position);
        for (int x = 0; x < tex.width; x++)
        {
            for (int y = 0; y < tex.height; y++)
            {

                if (eraser.GetComponent<BoxCollider2D>().bounds.Contains(new Vector2(
                    transform.position.x + x - (tex.width / 2),
                    transform.position.y + y - (tex.height / 2)
                ))) {
                    Debug.Log("MATCH");
                    if (colorArray[x + (y * tex.width)].a > 0)
                    {
                        colorArray[x + (y * tex.width)].a -= Random.Range(0.01f, 0.05f);
                        if (colorArray[x + (y * tex.width)].a < 0)
                        {
                            colorArray[x + (y * tex.width)].a = 0;
                        }
                    }
                }
            }
        }
        tex.SetPixels(colorArray);
        tex.Apply();
    }


}
