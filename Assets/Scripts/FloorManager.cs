﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour {

    public GameObject alma;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (alma.transform.GetChild(0).gameObject.GetComponent<Animator>().GetBool("usingBroom") == true)
        {
            foreach (Transform child in gameObject.transform)
            {
                if (child.GetComponent<BoxCollider2D>().IsTouching(alma.GetComponent<Player>().hand.GetComponent<BoxCollider2D>()))
                {
                    child.GetComponent<floorTask_wetFloor>().currentHP--;
                }
            }

            foreach (GameObject liquidMess in GameObject.FindGameObjectsWithTag("LiquidMess"))
            {
                liquidMess.GetComponent<TestPuddle>().eraseByBoxCollider(alma.GetComponent<Player>().hand, 1);
            }

        }
	}
}
