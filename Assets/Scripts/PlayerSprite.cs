﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprite : MonoBehaviour {

    public float bobIntensity = 100;
    public float bob;
    
    public void bobWhileMoving() {
        transform.localPosition = new Vector3(
            0,
            0 + (bob / bobIntensity) + 16,
            0
        );
    }

}
