﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MU : MonoBehaviour {

    private SpriteRenderer mySpriteRenderer;

    public GameObject player;
    public int maxDistanceToPlayer = 15;
    public int speed = 15;
    public int coreOffsetY = -10;

	// Use this for initialization
	void Start () {
        mySpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        float step = speed * Time.deltaTime;

        Vector2 normalizedLocation = new Vector2(
            player.transform.position.x,
            player.transform.position.y - coreOffsetY
        );

        if (Vector2.Distance(transform.position, normalizedLocation) > maxDistanceToPlayer)
        {
            if (transform.position.x > normalizedLocation.x)
            {
                mySpriteRenderer.flipX = false;
            }
            else
            {
                mySpriteRenderer.flipX = true;
            }
            transform.position = Vector2.MoveTowards(transform.position, normalizedLocation, step);
        }
	}
}
