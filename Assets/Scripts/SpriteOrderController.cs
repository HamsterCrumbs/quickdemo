﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteOrderController : MonoBehaviour {
    
    public int offsetY = 0;
    public bool inheritObjectZorder = false;
    public bool randomRotateOnStart = false;
    public GameObject targetObject;

    private SpriteRenderer mySpriteRenderer;

	// Use this for initialization
	void Start () {
        mySpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        if(randomRotateOnStart)
            transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
    }
	
	// Update is called once per frame
	void Update () {
        if(!inheritObjectZorder)
            mySpriteRenderer.sortingOrder = (int)Mathf.Round((transform.position.y + offsetY )* -1);
        else
            mySpriteRenderer.sortingOrder = (int)Mathf.Round((targetObject.transform.position.y + offsetY) * -1);
    }
}
