﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage : EquipScript {

    public int cost = 10;

    public override bool drop()
    {

        bool hasGarbageBag = false;
        
        Debug.Log(GameObject.FindGameObjectsWithTag("GarbageBag").Length);

        foreach (GameObject garbageBag in GameObject.FindGameObjectsWithTag("GarbageBag"))
        {
            if (garbageBag.GetComponent<BoxCollider2D>().IsTouching(transform.parent.transform.parent.GetComponent<BoxCollider2D>()))
            {
                if (garbageBag.GetComponent<GarbageBag>().capacity >= garbageBag.GetComponent<GarbageBag>().currentCapacity + cost)
                {
                    hasGarbageBag = true;
                    garbageBag.GetComponent<GarbageBag>().currentCapacity += cost;
                    base.drop();
                    Destroy(gameObject);
                    return true;
                }
                else
                {
                    //LET OUT "CANT PUT IN" SIGN
                    Debug.Log("garbage full");
                    hasGarbageBag = true;
                    return false;
                }
            }
        }

        if (!hasGarbageBag)
        {
            base.drop();
            return true;
        }
        return false;
    }
}
