﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_Broom : EquipScript {

    public override void use()
    {
        myAnimator.SetBool("usingBroom", true);

        if (SpriteXOffset < 0)
            xOffsetModifier = xOffsetModifier * -1;
        transform.localPosition = new Vector2((SpriteXOffset + (xOffsetModifier)) * isFlipped, SpriteYOffset);
    }

    public override void stopUse()
    {
        myAnimator.SetBool("usingBroom", false);
        transform.localPosition = new Vector2(SpriteXOffset * isFlipped, SpriteYOffset);
    }

    public override bool drop()
    {
        base.drop();
        transform.rotation = Quaternion.Euler(0, 0, 75);
        return true;
    }
}
