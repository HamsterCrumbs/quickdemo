﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipScript : MonoBehaviour
{

    public bool inUse;
    protected Animator myAnimator;
    public BoxCollider2D myBox;
    protected Rigidbody2D myRigidBody;

    protected Vector3 origPosition;
    protected Vector3 actualPosition;
    protected float xOffsetModifier;
    protected int isFlipped = -1;

    //ANIMATION OFFSET
    public float xOffset = 0;


    public float SpriteXOffset = 0;
    public float SpriteYOffset = 0;

    public float box_xOffset = 0;
    public float box_yOffset = 0;


    void Start()
    {
        if (transform.parent == null)
        {
            transform.GetComponent<SpriteOrderController>().inheritObjectZorder = false;
            transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
        else
        {
            myAnimator = transform.parent.GetComponent<Animator>();
        }
        myBox = transform.GetComponent<BoxCollider2D>();
        myRigidBody = transform.GetComponent<Rigidbody2D>();

        //myRigidBody.simulated = false;
    }

    void Update()
    {
        xOffsetModifier = xOffset;
    }


    public virtual void use()
    {
    }

    public virtual void stopUse()
    {
    }

    public virtual bool drop()
    {
        flip(true);
        transform.position = transform.parent.parent.position;

        transform.parent = null;
        transform.GetComponent<SpriteOrderController>().targetObject = null;
        transform.GetComponent<SpriteOrderController>().inheritObjectZorder = false;
        transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

        return true;
    }

    public virtual void grab(GameObject grabber)
    {
        transform.GetComponent<SpriteOrderController>().targetObject = grabber;
        transform.GetComponent<SpriteOrderController>().inheritObjectZorder = true;
        transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;

        flip(transform.parent.parent.GetComponent<Player>().isFlipped);
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void flip(bool f)
    {
        transform.GetComponent<SpriteRenderer>().flipX = f;

        float b_x = SpriteXOffset;
        float bc_x = box_xOffset;
        isFlipped = 1;

        if (!f)
        {
            b_x = SpriteXOffset * -1;
            bc_x = box_xOffset * -1;
            isFlipped = -1;
        }

        transform.localPosition = new Vector2(b_x, SpriteYOffset);
        myBox.offset = new Vector2(bc_x, box_yOffset);
    }
}
