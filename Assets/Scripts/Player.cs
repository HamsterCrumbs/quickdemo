﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Animator myAnimator;
    private SpriteRenderer mySpriteRenderer;
    public GameObject hand;
    private EquipScript handScript;
    public bool isFlipped = false;
    public bool isIdle = true;

    public GameObject[] inventory;

    private Vector3 origPosition;
    private Vector3 actualPosition;
    
    public float speed = 1;

	// Use this for initialization
	void Start () {
        myAnimator = transform.GetChild(0).gameObject.GetComponent<Animator>();
        mySpriteRenderer = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();

        hand = transform.GetChild(0).transform.GetChild(0).gameObject;
        handScript = hand.GetComponent<EquipScript>();
    }
    
	void Update ()
    {
        if (hand == null || !handScript.inUse)
        {
            move();
        }

        if (hand != null)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                handScript.use();
                isIdle = false;
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                handScript.stopUse();
                isIdle = true;
            }

            if (Input.GetKeyDown(KeyCode.G) && !handScript.inUse)
            {
                drop();
            }
        }
        if (hand == null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                myAnimator.SetBool("isCrouching", true);
                isIdle = false;
            }

            if (Input.GetKeyUp(KeyCode.E))
            {
                myAnimator.SetBool("isCrouching", false);
                isIdle = true;
                grab();
            }
        }
    }

    void drop()
    {
        bool success = handScript.drop();

        if (success)
        {
            handScript = null;
            hand = null;
        }
    }

    void grab()
    {
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Grabbable"))
        {
            if (transform.GetComponent<BoxCollider2D>().IsTouching(g.GetComponent<BoxCollider2D>()))
            {
                Debug.Log(transform.GetComponent<BoxCollider2D>().IsTouching(g.GetComponent<BoxCollider2D>()));
                hand = g;
                handScript = hand.GetComponent<EquipScript>();

                hand.transform.parent = transform.GetChild(0);

                handScript.grab(gameObject);
                break;
            }
        }
    }

    void move()
    {

        float dirX = 0, dirY = 0;

        if (Input.GetKey(KeyCode.A))
        {
            dirX = -1;
            if(isFlipped)
                flip(isFlipped = !isFlipped);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            dirX = 1;
            if(!isFlipped)
                flip(isFlipped = !isFlipped);
        }

        if (Input.GetKey(KeyCode.W))
        {
            dirY = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            dirY = -1;
        }

        if (dirX != 0 || dirY != 0)
        {
            myAnimator.SetBool("isMoving", true);
        }
        else
        {
            myAnimator.SetBool("isMoving", false);
        }

        if (dirX != 0 && dirY != 0) {
            dirX *= 0.7f;
            dirY *= 0.7f;
        }

        transform.position = new Vector3(
            transform.position.x + (speed * dirX),
            transform.position.y + (speed / 2 * dirY),
            transform.position.z
        );

        transform.GetChild(0).GetComponent<PlayerSprite>().bobWhileMoving();
    }

    void flip(bool isFlipped)
    {
        mySpriteRenderer.flipX = isFlipped;
        if(handScript != null)
            hand.GetComponent<EquipScript>().flip(isFlipped);
    }
}

