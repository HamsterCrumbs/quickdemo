﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	
    public Vector2 offset;
    public GameObject target;

	void Awake () {
	}

    private void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x + offset.x, target.transform.position.y + offset.y, -10), 0.3f);
    } 
}
