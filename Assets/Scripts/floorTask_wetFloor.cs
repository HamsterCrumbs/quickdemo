﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floorTask_wetFloor : MonoBehaviour {

    public int maxHP = 200;
    public int currentHP;
    private Animator myAnimator;


    // Use this for initialization
    void Start ()
    {
        currentHP = maxHP;
        myAnimator = gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        myAnimator.SetFloat("HPRate", (float)currentHP/maxHP);
        if (currentHP < 0) {
            Destroy(gameObject);
        }
	}
}
