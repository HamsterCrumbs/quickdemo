﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageBag : MonoBehaviour {

    private Animator myAnimator;
    public int capacity = 100;
    public int currentCapacity = 0;


	// Use this for initialization
	void Start () {
		myAnimator = transform.gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        myAnimator.SetFloat("cur_capacity", currentCapacity);
	}
}
